-- This script will create database 'foodorder' and user 'hrana' with privileges to 'foodorder' db.
-- Remember to modify src/main/resources/application.properties to disable embedded db
CREATE DATABASE foodorder;
CREATE USER 'hrana'@'localhost' IDENTIFIED BY 'hrana';
REVOKE CREATE ROUTINE, CREATE VIEW, CREATE USER, ALTER, SHOW VIEW, CREATE, ALTER ROUTINE, EVENT, SUPER, INSERT, RELOAD, SELECT, DELETE, FILE, SHOW DATABASES, TRIGGER, SHUTDOWN, REPLICATION CLIENT, GRANT OPTION, PROCESS, REFERENCES, UPDATE, DROP, REPLICATION SLAVE, EXECUTE, LOCK TABLES, CREATE TEMPORARY TABLES, INDEX ON *.* FROM 'hrana'@'localhost';
GRANT CREATE ROUTINE, CREATE VIEW, ALTER, SHOW VIEW, CREATE, ALTER ROUTINE, EVENT, INSERT, SELECT, DELETE, TRIGGER, REFERENCES, UPDATE, DROP, EXECUTE, LOCK TABLES, CREATE TEMPORARY TABLES, INDEX ON `foodorder`.* TO 'hrana'@'localhost';
REVOKE GRANT OPTION ON `foodorder`.* FROM 'hrana'@'localhost';
FLUSH PRIVILEGES;
