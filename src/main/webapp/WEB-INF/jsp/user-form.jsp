<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="header.jsp" %>

    <div class="title">
        <h2>Registracija</h2>
        <span class="byline">Registrirajte se!</span>
    </div>
    <p>
    <form:form commandName="user" method="POST">
        <table>
            <tr>
                <td>Ime:</td>
                <td><form:input path="name" /></td>
            </tr>
            <tr>
                <td>E-mail:</td>
                <td><form:input path="email" /></td>
            </tr>
            <tr>
                <td>Password:</td>
                <td><form:password path="password"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Save Changes" />
                </td>
            </tr>
        </table>
    </form:form>
    </p>


<%@ include file="footer.jsp" %>