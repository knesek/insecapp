<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="header.jsp" %>

<div class="title">
    <h2>Playground</h2>
</div>
    <!--
        model.addAttribute("var", var);
		model.addAttribute("varHtml1", ESAPI.encoder().encodeForHTML(var));
		model.addAttribute("varHtml2", StringEscapeUtils.escapeHtml(var));
		model.addAttribute("varAttr", ESAPI.encoder().encodeForHTMLAttribute(var));
		model.addAttribute("varJs1", escapeJs(var));
		model.addAttribute("varJs2", ESAPI.encoder().encodeForJavaScript(var));
		model.addAttribute("varJs3", StringEscapeUtils.escapeJavaScript(var));
    -->
<div style="margin-top: 30px; margin-bottom: 30px">
    <h3>Testiraj varijablu</h3>
    <form method="get">
        <input name="var" type="text">
        <input type="submit" value="Testiraj">
    </form>
</div>


<!-- JAVASCRIPT TESTING -->
<div>
    <script type="text/javascript">
        function setVar() {

            var someVar=${varJs2};
            document.getElementsByName("testInput").item(0).setAttribute("value", someVar);
        }
    </script>
    <input name="testInput" type="text"> <button onclick="setVar()">Show</button>

</div>

<%--
<!-- HTML TAG TESTING -->
<div>
    <div>Hello ${var}!</div>
</div>
--%>

<%--
<!-- HTML ATTRIBUTE TESTING -->
<div>
    <div>Hello from <input type="text" value=${varAttr} ></div>
</div>
--%>
<%@ include file="footer.jsp" %>