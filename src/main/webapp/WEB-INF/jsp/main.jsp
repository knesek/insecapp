<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="header.jsp" %>

            <div class="title">
                <h2>Diverto Narudžbe Hrane</h2>
                <span class="byline">H<b>ranjiva</b> aplikacija - Najsigurniji način da naručite hranu!</span>
            </div>
            <p style="margin-top: 20px"> Diverto Hrana omogućuje vam jednostavno i zabavno naručivanje hrane. </p>
            <h3>&nbsp;</h3>
            <h3>Kako naručiti hranu</h3>
            <p> Bez brige, ne morate biti ekspert da bi naručili hranu. Diverto Hrana funkcionira
                vrlo jednostavno, samo se registrirajte i naručite! </p>
            <form method="GET" action="search">
                <p>Pretražite našu hranu: <input name="q" type="text" value="${param.q}" />
                <input type="submit" value="Traži!" />
            </form>
            <c:if test="${param.q != null}">
                    Rezultati pretrage za <b><c:out value="${param.q}" /></b>:
                    <ul style="margin-top: 10px">
                    <c:forEach items="${foundFoods}" var="food">
                        <li>${food}</li>
                    </c:forEach>
                    </ul>
            </c:if>

            <h3>Tko trenutno naručuje?</h3>
<p>
                <c:choose>
                    <c:when test="${loggedInUsers.size() > 0}">
                        Trenutno naručuju:
                        <c:forEach items="${loggedInUsers}" var="usr">
                            <input type="text" value="<c:out value="${usr.name}" />">


                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        Ovog trenutka nitko ne naručuje ništa.
                    </c:otherwise>
                </c:choose>
            </p>


<%@ include file="footer.jsp" %>