<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="header.jsp" %>

    <div class="title">
        <h2>Diverto Narudžbe Hrane - Jeste li spremni?</h2>
        <span class="byline">H<b>ranjiva</b> aplikacija - Najsigurniji način da naručite hranu!</span>
    </div>
    <p style="margin-top: 20px"> Diverto Hrana omogućuje vam jednostavno i zabavno naručivanje hrane. </p>
    <c:if test="${orderedFood != null}">
        <h3>Naručili ste: ${orderedFood}!</h3>
        <h3>&nbsp;</h3>
        <h3>Naruči još!</h3>
    </c:if>
    <form method="POST"  action="<c:url value="/food-order/submit"/>">
        <select name='foodName'>
            <c:forEach items="${availableFood}" var="food">
                <option value="${food.name}">${food.name} - ${food.price} kn</option>
            </c:forEach>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </select>
        <input type="submit" value="Naruči!">
    </form>
    <h3>&nbsp;</h3>
    <h3>Naručite hranu odabirom s liste - vidi iznad! </h3>
    <p> Bez brige, ne morate biti ekspert da bi naručili hranu. Samo odaberite jelo s liste i kliknite naruči!</p>
    <c:if test="${recentOrders.size() > 0}">
        <h3>Nedavne narudžbe:</h3>
        <ul>
            <c:forEach items="${recentOrders}" var="order">
                <li>${order.food.name} za ${order.food.price} kn (${order.dateOrdered})
                    [ <a href="<c:url value="/food-order/cancel/${order.id}" />">Otkaži</a> ]</li>
            </c:forEach>
        </ul>
    </c:if>

<%@ include file="footer.jsp" %>