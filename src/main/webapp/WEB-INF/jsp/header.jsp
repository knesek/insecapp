<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,600,700" rel="stylesheet" />
    <link href="<c:url value="/css/default.css"/>" rel="stylesheet" type="text/css" media="all" />
</head>
<body>

<div id="header" class="container">
    <div id="logo">
        <h1><a href="<c:url value="/" />">Diverto Hrana</a></h1>
    </div>
    <div id="menu">
        <ul>
            <li class="current_page_item"><a href="<c:url value="/food-order/"/>" accesskey="1" title="">Narudžba</a></li>
            <li><a href="<c:url value="/register" />" accesskey="2" title="">Registracija</a></li>
            <li><a href="<c:url value="/login" />" accesskey="2" title="">Login</a></li>
        </ul>
    </div>
</div>
<div id="wrapper">
    <div id="page" class="container">
        <div id="content">
        <c:if test="${flash != null}">
            <div style="background: #E4F3E8; color: black; padding: 5px; margin-bottom: 20px; border: dashed 1px;">
                    ${flash}
            </div>
        </c:if>