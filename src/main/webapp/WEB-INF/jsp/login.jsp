<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="header.jsp" %>

    <div class="title">
        <h2>Login</h2>
        <span class="byline">Ulogirajte se da naručite hranu!</span>
    </div>
    <p style="margin-top: 20px"></p>
    <c:if test="${param.error}">
        <div style="background: darkred; color: white">
            Pogrešni e-mail ili password.
        </div>
    </c:if>
    <form method="POST" action="<c:url value="/j_spring_security_check" />" >
        <table>
            <tr>
                <td>E-mail:</td>
                <td><input type="text" name="username" /></td>
            </tr>
            <tr>
                <td>Password:</td>
                <td><input type="password" name="password"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Login" />
                </td>
            </tr>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </table>
    </form>

<%@ include file="footer.jsp" %>