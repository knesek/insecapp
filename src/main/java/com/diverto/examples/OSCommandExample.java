package com.diverto.examples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * @author knesek
 * Created on: 2/16/14
 */
public class OSCommandExample {

	private final static Logger log = LoggerFactory.getLogger(OSCommandExample.class);

	public static void main(String[] args) throws Exception {
		OSCommandExample example = new OSCommandExample();
		example.backupAfterUpload("file.txt|ls /etc");
		//example.backupAfterUpload("file.txt|ls");
	}

	public String executeOSCommand(String command) throws IOException {
		Runtime runtime = Runtime.getRuntime();
		Process proc = runtime.exec(command);
		try {
			proc.waitFor();
		} catch (InterruptedException e) {
			log.debug("Exception occured waiting for process to finish " + e.getMessage(), e);
		}
		String result = readStreamToString(proc.getInputStream());
		result += readStreamToString(proc.getErrorStream());
		return result;
	}

	public String runShellScript(String script) throws IOException {
		Runtime runtime = Runtime.getRuntime();
		Process proc = runtime.exec("/bin/bash file.txt|ls");
		OutputStream out = proc.getOutputStream();
		OutputStreamWriter scriptWriter = new OutputStreamWriter(out);
		scriptWriter.write(script);
		scriptWriter.close();
		out.close();
		try {
			proc.waitFor();
		} catch (InterruptedException e) {
			log.debug("Exception occured waiting for " +
					"process to finish " + e.getMessage(), e);
		}
		String result = readStreamToString(proc.getInputStream());
		result += readStreamToString(proc.getErrorStream());
		return result;
	}

	private String readStreamToString(InputStream is) throws IOException {
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		StringBuilder result = new StringBuilder();
		String line;
		while ((line = br.readLine()) != null) {
			result.append(line).append("\n");
		}
		isr.close();
		return result.toString();
	}

	public void backupAfterUpload(String fileName) throws IOException {
		String output = executeOSCommand("/bin/bash backup.sh " + fileName);
		//String output = runShellScript("./backup.sh " + fileName);
		log.info("Running backup script with file " + fileName + ". Output: " + output);
	}
}
