package com.diverto.examples;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * @author knesek
 * Created on: 2/16/14
 */
public class PreparedStatmentExample {

	public void preparedStatementExample(Long id, Connection con) throws SQLException {
		String query = "select content from pages where id = ?";
		PreparedStatement prepStmt = con.prepareStatement(query);
		prepStmt.setLong(1, id);
		ResultSet rs = prepStmt.executeQuery();
	}

	public void entityManagerExample(Long id, EntityManager entityManager) {
		String query = "select content from pages where id = ?";
		Query jpaQuery = entityManager.createNativeQuery(query);
		jpaQuery.setParameter(1, id);
		jpaQuery.getSingleResult();
	}

	public void hibernateExample(Long id, Session session) {
		String query = "select content from pages where id = :id";
		SQLQuery sqlQuery = session.createSQLQuery(query);
		sqlQuery.setParameter("id", id);
		sqlQuery.list();
	}

}
