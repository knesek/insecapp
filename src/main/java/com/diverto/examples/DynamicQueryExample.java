package com.diverto.examples;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * @author knesek
 * Created on: 2/16/14
 */
public class DynamicQueryExample {


	//URL example.com/getTransactions.page?__minAmount&__amount=1000
	public String buildDynamicQuery1(HttpServletRequest request) {
		String query = "select * from account_transactions where user = ? ";
		Enumeration<String> paramNames = request.getParameterNames();
		while (paramNames.hasMoreElements()) {
			String paramName = paramNames.nextElement();
			if (paramName.startsWith("__")) {
				String param = paramName.substring(1);
				query += " and " + param + " = " +
						request.getParameter(paramName);
			}
		}
		return query;
	}

	//URL example.com/getTableData.json?_table=account_transactions&_page=1
	public Query buildDynamicQuery2(HttpServletRequest request,
									EntityManager entityManager, int pageSize) {
		String table = request.getParameter("_table");
		Integer page = Integer.parseInt(request.getParameter("_page"));
		String query = "select * from " + table + " where page = ?";
		return entityManager.createNativeQuery(query)
				.setParameter(1, page)
				.setFirstResult(page * pageSize)
				.setMaxResults(pageSize);
	}
}
