package com.diverto.insecapp.web;

import com.diverto.insecapp.model.Food;
import com.diverto.insecapp.model.FoodOrder;
import com.diverto.insecapp.model.User;
import com.diverto.insecapp.repositories.FoodOrderRepository;
import com.diverto.insecapp.repositories.FoodRepository;
import com.diverto.insecapp.service.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * @author knesek
 * Created on: 1/27/14
 */
@Controller
@RequestMapping("/food-order")
public class FoodOrderController {

	private final static Logger log = LoggerFactory.getLogger(FoodOrderController.class);

	@Autowired
	UserInfo userInfo;

	@Autowired
	FoodOrderRepository foodOrderRepository;

	@Autowired
	FoodRepository foodRepository;

	@PersistenceContext
	EntityManager entityManager;

	@ModelAttribute("recentOrders")
	public List<FoodOrder> getRecentOrders() {
		User user = userInfo.getUser();
		List<FoodOrder> result = new ArrayList<FoodOrder>();
		if (user != null) {
			result = foodOrderRepository.findByUser(user.getId(), new PageRequest(0, 5, new Sort(Sort.Direction.DESC, "dateOrdered")));
		}
		return result;
	}

	@ModelAttribute("availableFood")
	public List<Food> getAvailableFood() {
		return foodRepository.findAll();
	}

	@RequestMapping("/")
	public String index() {
		return "food-order";
	}

	@Transactional
	@RequestMapping("/cancel/{id}")
	public String cancelOrder(@PathVariable("id") String id, RedirectAttributes redirectAttributes) {
		String findNameSql = "select f.name from food f, foodorder o where f.id = o.food_id and o.id = " + id;
		String deletedFoodOrderName = "" + entityManager.createNativeQuery(findNameSql).setMaxResults(1).getSingleResult();
		String deleteOrderSql = "delete from foodorder where id = " + id;
		entityManager.createNativeQuery(deleteOrderSql).executeUpdate();
		redirectAttributes.addFlashAttribute("flash", "Narudžba za " + deletedFoodOrderName + " je otkazana!");
		return "redirect:/food-order/";
	}

	@RequestMapping(value = "/submit", method = RequestMethod.POST)
	public String saveOrder(FoodOrderForm foodOrderForm, Model model) {

		String sql = "select id, name from food where name = '" + foodOrderForm.foodName + "'";
		Query query = entityManager.createNativeQuery(sql).setMaxResults(1);
		Object[] result = (Object[]) query.getSingleResult();
		model.addAttribute("orderedFood", result[1]);
		Food food = entityManager.find(Food.class, ((Number) result[0]).longValue());
		FoodOrder foodOrder = new FoodOrder();
		foodOrder.setFood(food);
		foodOrder.setOrderedBy(userInfo.getUser());
		foodOrderRepository.save(foodOrder);
		model.addAttribute("recentOrders", getRecentOrders());
		return "food-order";
	}

	public static class FoodOrderForm {
		private String foodName;

		public String getFoodName() {
			return foodName;
		}

		public void setFoodName(String foodName) {
			this.foodName = foodName;
		}
	}
}
