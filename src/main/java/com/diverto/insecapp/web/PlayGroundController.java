package com.diverto.insecapp.web;

import com.diverto.insecapp.model.Food;
import org.apache.commons.lang.StringEscapeUtils;
import org.owasp.esapi.ESAPI;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Arrays;
import java.util.List;

/**
 * @author knesek
 * Created on: 2/20/14
 */
@Controller
public class PlayGroundController {

	@PersistenceContext
	EntityManager entityManager;

	@RequestMapping("playground")
	public String playground(@RequestParam(value = "var", defaultValue = "", required = false) String var, Model model) {
		model.addAttribute("var", var);
		model.addAttribute("varHtml1", ESAPI.encoder().encodeForHTML(var));
		model.addAttribute("varHtml2", StringEscapeUtils.escapeHtml(var));
		model.addAttribute("varAttr", ESAPI.encoder().encodeForHTMLAttribute(var));
		model.addAttribute("varJs1", escapeJs(var));
		model.addAttribute("varJs2", ESAPI.encoder().encodeForJavaScript(var));
		model.addAttribute("varJs3", StringEscapeUtils.escapeJavaScript(var));

		return "playground";
	}

	private String escapeJs(String str) {
		str = str.replace("\\", "\\\\");
		str = str.replace("'", "\\'");
		str = str.replace("\"", "\\\"");
		return str;
	}


	@RequestMapping("/testQuery")
	public String testIn() {
		List<Long> ids = Arrays.asList(1l, 2l, 3l);
		List<Food> results = entityManager.createQuery("select f from Food f where f.id in :idList", Food.class)
				.setParameter("idList", ids)
				.getResultList();
		return "main";
	}

}
