package com.diverto.insecapp.web;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.codecs.MySQLCodec;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import java.util.List;

/**
 * @author knesek
 * Created on: 2/19/14
 */
@Controller
public class StoredProcedureDemoController {

	@PersistenceContext
	EntityManager entityManager;

	@RequestMapping("testStored1")
	@ResponseBody
	public List<Object[]> demoProcedure1(@RequestParam("food") String foodName) {


		foodName = ESAPI.encoder()
				.encodeForSQL(new MySQLCodec(MySQLCodec.Mode.STANDARD), foodName);

		StoredProcedureQuery query = entityManager
				.createStoredProcedureQuery("get_orders_for_food");
		query.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		query.setParameter(1, foodName);
		List<Object[]> results = query.getResultList();
		return results;
	}

	@RequestMapping("testStored2")
	@ResponseBody
	public List<Object[]> demoProcedure2(@RequestParam("food") String foodName) {
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("get_orders_for_food2");
		query.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		query.setParameter(1, foodName);
		List<Object[]> results = query.getResultList();
		return results;
	}

}
