package com.diverto.insecapp.web;

import com.diverto.insecapp.model.User;
import com.diverto.insecapp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author knesek
 * Created on: 1/26/14
 */
@Controller
public class IndexController {

	@Autowired
	UserRepository userRepository;

	@PersistenceContext
	EntityManager entityManager;

	@Autowired
	private SessionRegistry sessionRegistry;

	@RequestMapping("/")
	public String index(Model model) {
		model.addAttribute("loggedInUsers", sessionRegistry.getAllPrincipals());
		return "main";
	}

	@Transactional
	@RequestMapping("/search")
	public String searchIndex(@RequestParam(value = "q", required = true) String searchText, Model model) {
		String foodSearchQuery = "select name from Food f " +
				"where lower(f.name) like '%" + searchText.toLowerCase() +"%'";
		Query query = entityManager.createNativeQuery(foodSearchQuery);
		List<?> foundFoods = query.getResultList();
		model.addAttribute("loggedInUsers", sessionRegistry.getAllPrincipals());
		model.addAttribute("foundFoods", foundFoods);
		return "main";
	}

	@RequestMapping("/login")
	public String login() {
		return "login";
	}

	@RequestMapping("/users")
	@ResponseBody
	public List<User> listUsers() {
		return userRepository.findAll();
	}

	/*
	@RequestMapping("/getUser")
	@ResponseBody
	public User user(@RequestParam("id") String id) {
		return (User) entityManager.createNativeQuery("select * from user where id = " + id, User.class).getSingleResult();
	}

	@RequestMapping("/userCount")
	@ResponseBody
	public Integer count() {
		return ((Number) entityManager.createNativeQuery("select count(*) from user").getSingleResult()).intValue();
	}*/

}
