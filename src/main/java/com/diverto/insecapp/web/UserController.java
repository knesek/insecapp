package com.diverto.insecapp.web;

import com.diverto.insecapp.model.User;
import com.diverto.insecapp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @author knesek
 * Created on: 1/27/14
 */
@Controller
public class UserController {

	@Autowired
	UserRepository userRepository;

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView newUserForm() {
		User user = new User();
		ModelAndView mav = new ModelAndView("user-form");
		mav.addObject("user", user);
		return mav;
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String saveUser(User user, RedirectAttributes redirectAttributes) {
		userRepository.save(user);
		redirectAttributes.addFlashAttribute("flash", "Registracija uspješna! Ulogirajte se s vašim e-mailom i lozinkom!");
		return "redirect:/login";
	}


}
