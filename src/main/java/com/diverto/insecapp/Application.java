package com.diverto.insecapp;

import com.diverto.insecapp.model.User;
import com.diverto.insecapp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import javax.servlet.Filter;
import javax.sql.DataSource;
import java.io.File;
import java.sql.SQLException;

/**
 * @author knesek
 * Created on: 1/26/14
 */
@ComponentScan
@EnableAutoConfiguration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.diverto.insecapp.repositories")
@PropertySource("classpath:application.properties")
public class Application extends SpringBootServletInitializer implements EmbeddedServletContainerCustomizer {

	@Autowired
	UserDetailsService userDetailsService;

	@Autowired
	@Qualifier("sessionRegistry")
	SessionRegistry sessionRegistry;

	@Autowired
	@Order(Ordered.HIGHEST_PRECEDENCE)
	Environment env;

	@Override
	public void customize(ConfigurableEmbeddedServletContainerFactory factory) {
		factory.setDocumentRoot(new File("webapp"));
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyPlaceHolderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
		UserRepository userRepository = context.getBean(UserRepository.class);
		userRepository.save(new User("Kreso", "knesek@bla.com", "pass"));
		userRepository.save(new User("Marko", "marko@bla.com", "pass2"));
	}

	@Bean
	public DataSource dataSource(Environment env) {
		if (!env.getProperty("spring.datasource.useEmbeddedDb", Boolean.class, false)) {
			org.apache.tomcat.jdbc.pool.DataSource dataSource = new org.apache.tomcat.jdbc.pool.DataSource();
			String driverClassName = env.getProperty("spring.datasource.driverClassName");
			dataSource.setDriverClassName(driverClassName);
			dataSource.setUrl(env.getProperty("spring.datasource.url"));
			dataSource.setUsername(env.getProperty("spring.datasource.username"));
			dataSource.setPassword(env.getProperty("spring.datasource.password"));
			return dataSource;
		} else {
			return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2).build();
		}
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, JpaVendorAdapter
		jpaVendorAdapter) {
		LocalContainerEntityManagerFactoryBean lef = new LocalContainerEntityManagerFactoryBean();
		lef.setDataSource(dataSource);
		lef.setJpaVendorAdapter(jpaVendorAdapter);
		lef.setPackagesToScan("com.diverto.insecapp.model");
		return lef;
	}

	@Bean
	public JpaVendorAdapter jpaVendorAdapter(Environment env) {
		HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
		hibernateJpaVendorAdapter.setShowSql(false);
		hibernateJpaVendorAdapter.setGenerateDdl(true);
		if (env.getProperty("spring.datasource.useEmbeddedDb", Boolean.class, false)) {
			hibernateJpaVendorAdapter.setDatabase(Database.H2);
		} else {
			hibernateJpaVendorAdapter.setDatabase(Database.MYSQL);
		}

		return hibernateJpaVendorAdapter;
	}



	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/jsp/");
		viewResolver.setSuffix(".jsp");
		viewResolver.setContentType("text/html;charset=utf-8");
		viewResolver.setOrder(0);
		return viewResolver;
	}


	@Bean
	public PlatformTransactionManager transactionManager() {
		return new JpaTransactionManager();
	}

	@Bean
	public Filter charsetFilter() {
		CharacterEncodingFilter filter = new CharacterEncodingFilter();
		filter.setEncoding("utf-8");
		filter.setForceEncoding(true);
		return filter;
	}

	@Bean
	public ApplicationSecurity applicationSecurity() {
		return new ApplicationSecurity();
	}

	@Bean(name = "sessionRegistry")
	public SessionRegistry sessionRegistry() {
		SessionRegistry sessionRegistry = new SessionRegistryImpl();
		return sessionRegistry;
	}

	@Order(Ordered.LOWEST_PRECEDENCE - 8)
	protected class ApplicationSecurity extends WebSecurityConfigurerAdapter {
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.formLogin()
					.loginPage("/login")
					.defaultSuccessUrl("/")
					.failureUrl("/login?error=true")
					.usernameParameter("username")
					.passwordParameter("password")
					.loginProcessingUrl("/j_spring_security_check")
					.permitAll()
					.and()
				.logout()
					.logoutSuccessUrl("/index.jsp")
					.and()
				.sessionManagement()
					.maximumSessions(1)
						.sessionRegistry(sessionRegistry)
						.and()
					.and()
				.authorizeRequests()
					.antMatchers("/", "/search", "/register", "/login", "/test**", "/playground**", "/j_spring_security_check").permitAll()
					.antMatchers("/css/**", "/js/**", "/images/**", "/fonts/**").permitAll()
					.antMatchers("/admin/**").hasRole("ADMIN")
					.anyRequest().authenticated()
					.and()
				.userDetailsService(userDetailsService);
		}
	}
}
