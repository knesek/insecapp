package com.diverto.insecapp.repositories;

import com.diverto.insecapp.model.Food;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author knesek
 * Created on: 1/27/14
 */
@Repository
public interface FoodRepository extends JpaRepository<Food, Long>{

}
