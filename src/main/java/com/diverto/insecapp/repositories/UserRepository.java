package com.diverto.insecapp.repositories;

import com.diverto.insecapp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author knesek
 * Created on: 1/27/14
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
