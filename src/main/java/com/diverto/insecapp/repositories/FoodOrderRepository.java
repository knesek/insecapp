package com.diverto.insecapp.repositories;

import com.diverto.insecapp.model.FoodOrder;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author knesek
 * Created on: 1/27/14
 */
@Repository
public interface FoodOrderRepository extends JpaRepository<FoodOrder, Long> {

	@Query("select f from FoodOrder f join fetch f.food where f.orderedBy.id = ?1")
	List<FoodOrder> findByUser(Long userId, Pageable page);

}
