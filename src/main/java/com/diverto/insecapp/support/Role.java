package com.diverto.insecapp.support;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author knesek
 * Created on: 1/27/14
 */
public enum Role implements GrantedAuthority {
	User, Admin;

	public String getAuthority() {
		return name().toUpperCase();
	}

	@Override
	public String toString() {
		return getAuthority();
	}
}
