package com.diverto.insecapp.service;

import com.diverto.insecapp.model.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

/**
 * @author knesek
 * Created on: 1/27/14
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@PersistenceContext
	EntityManager entityManager;

	@Transactional
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Query findUserQuery = entityManager.createNativeQuery("SELECT * FROM user u WHERE u.email = '" + username + "'", User.class);
		try {
			User user = (User) findUserQuery.getSingleResult();
			return user;
		} catch (NoResultException ex) {
		} catch (NonUniqueResultException ex) {
		}
		throw new UsernameNotFoundException(username);
	}
}
