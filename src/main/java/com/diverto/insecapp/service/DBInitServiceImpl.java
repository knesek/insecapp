package com.diverto.insecapp.service;

import com.diverto.insecapp.model.Food;
import com.diverto.insecapp.repositories.FoodRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * @author knesek
 * Created on: 1/27/14
 */
@Service
public class DBInitServiceImpl {

	private final static Logger log = LoggerFactory.getLogger(DBInitServiceImpl.class);

	@Autowired
	FoodRepository foodRepository;

	@PostConstruct
	public void init() {
		if (foodRepository.count() == 0) {
			log.debug("Populating the db...");
			foodRepository.save(new Food("QUESADILLAS MACHACA", 	42));
			foodRepository.save(new Food("QUESADILLAS DURANGO", 	40));
			foodRepository.save(new Food("KEBAB NORMAL", 			29));
			foodRepository.save(new Food("KEBAB TORTILJA", 			29));
			foodRepository.save(new Food("KEBAB SPECIJAL", 			31));
			foodRepository.save(new Food("CHICKENBURGER", 			22));
			foodRepository.save(new Food("HAMBURGER", 				22));
			foodRepository.save(new Food("CHEESEBURGER", 			24));
			foodRepository.save(new Food("SLAVONSKA PIZZA", 		40));
			foodRepository.save(new Food("DALMATINSKA PIZZA", 		40));
			foodRepository.save(new Food("PIZZA AL TONO", 			40));
			foodRepository.save(new Food("PIZZA PICANTE", 			40));
			foodRepository.save(new Food("PIZZA MARGARITA", 		40));
			foodRepository.save(new Food("BEČKI ODREZAK ", 			45));
			foodRepository.save(new Food("ZAGREBAČKI ODREZAK", 		45));
			foodRepository.save(new Food("NARAVNI ODREZAK", 		45));
		}
	}
}