package com.diverto.insecapp.service;

import com.diverto.insecapp.model.User;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * @author knesek
 * Created on: 1/28/14
 */
@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.INTERFACES)
public class SpringSecurityUserInfo implements UserInfo {

	@Override
	public User getUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth.getPrincipal() != null && auth.getPrincipal() instanceof User) {
			return (User) auth.getPrincipal();
		}
		return null;
	}
}
