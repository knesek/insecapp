package com.diverto.insecapp.service;

import com.diverto.insecapp.model.User;

/**
 * @author knesek
 * Created on: 1/28/14
 */
public interface UserInfo {
	User getUser();
}
