package com.diverto.insecapp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

/**
 * @author knesek
 * Created on: 1/27/14
 */
@Entity
public class FoodOrder {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Date dateOrdered;

	@ManyToOne
	private User orderedBy;

	@ManyToOne
	private Food food;

	public FoodOrder() {
		dateOrdered = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public User getOrderedBy() {
		return orderedBy;
	}

	public void setOrderedBy(User orderedBy) {
		this.orderedBy = orderedBy;
	}

	public Food getFood() {
		return food;
	}

	public void setFood(Food food) {
		this.food = food;
	}
}
