-- vulnerabilty demo for stored procedures

/*
DROP PROCEDURE IF EXISTS get_orders_for_food2;
-- SEP
CREATE PROCEDURE get_orders_for_food2(foodName VARCHAR(255))
BEGIN
	select u.name as userName, f.name as foodName, ord.dateOrdered from User u, Food f, FoodOrder ord where u.id = ord.orderedBy_id and ord.food_id = f.id and f.name = foodName;
END
-- SEP
DROP PROCEDURE IF EXISTS get_orders_for_food;
-- SEP
CREATE PROCEDURE get_orders_for_food(foodName VARCHAR(255))
BEGIN
	SET @query = 'select u.name as userName, f.name as foodName, ord.dateOrdered from User u, Food f, FoodOrder ord where u.id = ord.orderedBy_id and ord.food_id = f.id and f.name = \'';
	SET @query = CONCAT(@query, foodName, '\'');
	PREPARE stmt FROM @query;
	EXECUTE stmt;
END
-- SEP
*/